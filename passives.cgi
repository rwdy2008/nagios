#!/usr/bin/perl
################################################################################
#
#	Program:	passives.cgi
#
#	Author:		Tim Schaefer, tim@maestrodata.com
#			asysarchitect@gmail.com
#
#	Description:	PASV CGI Receiver Script For Nagios Passive Services
#
#	Takes the input from a remote server plugin and dunks it into nagios.cmd
#
#	Latest:		Wed Sep  7 16:58:01 PST 2011
#
#	Usage:		./passives.cgi
#
# 		$nh - nagios host
# 		$ns - nagios service
# 		$nc - nagios status code
# 		$nm - nagios status message
#
################################################################################

	$|=1 ;

        use CGI ;

        $co                    = new CGI ;

	my $pasvserver_log_file   = '/var/log/passives.log' ;

	my $nagios_cmd       = "/var/spool/nagios/cmd/nagios.cmd" ;

	$port             = '80' ;

	my $status        = ""  ;
        my %known_hosts   = ""  ;

	my $epoch         = time ;
	my $timestamp     = cnvt_epoch_to_timestring ( $epoch ) ;



        my $nh = $co->param("nh");
           $nh = ($nh)?"$nh" : 'NULL' ;

        my $ns = $co->param("ns");
           $ns = ($ns)?"$ns" : 'NULL' ;

        my $nc = $co->param("nc");
           $nc = ($nc)?"$nc" : 'NULL' ;

        my $nm = $co->param("nm");
           $nm = ($nm)?"$nm" : 'NULL' ;

	if( $nh ne 'NULL' && $ns ne 'NULL' && $nc ne 'NULL' && $nm ne 'NULL' )
		{
		if ( $nc eq 'OK' )       { $nc = 0 ; }
		if ( $nc eq 'WARNING' )  { $nc = 1 ; }
		if ( $nc eq 'CRITICAL' ) { $nc = 2 ; }
		if ( $nc eq 'UNKNOWN' )  { $nc = 3 ; }

		$epoch     = time ;
		$timestamp = cnvt_epoch_to_timestring ( $epoch ) ;

		write_nagios_cmd( $nh, $ns, $nc, $nm ) ;
		print "Content-Type: text/html\n\nOK\n" ;
		}
	else	{
		print "Content-Type: text/html\n\nERROR - ARGS INVALID\n" ;
#		print PASVLOG "$status : ARGS INVALID\n" ;
		}

################################################################################

sub write_nagios_cmd {

	my $e       = time ;

	my $message = URLDecode( $nm ) ;

#	my $cmd_str = sprintf( "[%i] PROCESS_SERVICE_CHECK_RESULT;%s;%s;%s;%s\n %i" , $e, $nh, $ns,$nc,$message, $e ) ;
	my $cmd_str = sprintf( "[%i] PROCESS_SERVICE_CHECK_RESULT;%s;%s;%s;%s" , $e, $nh, $ns,$nc,$message ) ;

	open( NAGIOS_CMD, ">>$nagios_cmd" ) or die "$timestamp - Cannot open nagios command file [$nagios_cmd]\n" ;
	print NAGIOS_CMD "$cmd_str\n"  ;
	close( NAGIOS_CMD ) ;
	
## Disabling the log.
#	open( PASVLOG, ">>$pasvserver_log_file" ) ;
#	print PASVLOG "$cmd_str\n" ;
#	close(PASVLOG);
}
1;

################################################################################

sub URLDecode {
my $theURL = $_[0];
$theURL =~ tr/+/ /;
$theURL =~ s/%([a-fA-F0-9]{2,2})/chr(hex($1))/eg;
$theURL =~ s/<!–(.|\n)*–>//g;
return $theURL;

}
1;

################################################################################

sub cnvt_epoch_to_timestring {

        use Time::Local ;

        my $p_epoch = $_[0] ;

        my $sec   = 0 ;
        my $min   = 0 ;
        my $hour  = 0 ;
        my $mday  = 0 ;
        my $mon   = 0 ;
        my $year  = 0 ;
        my $wday  = 0 ;
        my $yday  = 0 ;
        my $isdst = 0 ;

        # 2-digit year
        # $year = sprintf("%02d", $year % 100);

        ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime( $p_epoch ) ;

        $year += 1900 ;
        $mon  += 1 ;

        $sec   = sprintf( "%2.2d", $sec ) ;
        $min   = sprintf( "%2.2d", $min ) ;
        $hour  = sprintf( "%2.2d", $hour ) ;

        my $timestamp = sprintf( "%4s-%2.2d-%2.2d %2.2d:%2.2d:%2.2d", $year,$mon,$mday,$hour,$min,$sec ) ;

        my $ret_time  = $timestamp ;

        return $ret_time ;
}
1;
