#!/bin/bash
##################################################
#                                                #
#     generate the hosts and service cfg file    #
#     2015-07-01 by pfwu                         #
#                                                #
##################################################

Mysql_Database="databasename"
Dir="/etc/nagios/kabam/hosts"

eval `mysql -N -D $Mysql_Database -e "select * from server;" | awk '{print "line="NR,"N["NR"]="$2,"IP["NR"]="$3}'`

for i in `seq 1 $line`
do
        Role=`echo ${N[$i]} | cut -d- -f3`
        if [[ $Role =~ "storage" ]];then
                cat > $Dir/${N[$i]}.cfg << EOF
                        define host {
                                use             linux-server 
                                host_name       ${N[$i]} 
                                alias           ${N[$i]}
                                address         ${IP[$i]}
                                hostgroups      manway
                                notes           ${N[$i]}
                        }
EOF
        else
                cat > $Dir/${N[$i]}.cfg << EOF
                        define host {
                                use             linux-server 
                                host_name       ${N[$i]} 
                                alias           ${N[$i]}
                                address         ${IP[$i]}
                                hostgroups      manway,manway_node
                                notes           ${N[$i]}
                        }
EOF
        fi
done